﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ExtratorInformacoesLicitacoes.Utils
{
   public static class ChromeDriverUtil
    {
        public static void WaitForJqueryAjax(ChromeDriver driver)
        {
            int delay = 1000;
            while (delay > 0)
            {
                Thread.Sleep(1000);
                var jquery = (bool)(driver as IJavaScriptExecutor)
                    .ExecuteScript("return window.jQuery == undefined");
                if (jquery)
                {
                    break;
                }
                var ajaxIsComplete = (bool)(driver as IJavaScriptExecutor)
                    .ExecuteScript("return window.jQuery.active == 0");
                if (ajaxIsComplete)
                {
                    break;
                }
                delay--;
            }
        }

        public static List<IWebElement> FindElementTree(ChromeDriver driver, By by)
        {
            List<IWebElement> tree = new List<IWebElement>();

            try
            {
                IWebElement element = driver.FindElement(@by);
                tree.Add(element); //starting element

                do
                {
                    element = element.FindElement(By.XPath("./parent::*")); //parent relative to current element
                    tree.Add(element);

                } while (element.TagName == "tr");
            }
            catch (NoSuchElementException)
            {
            }

            return new List<IWebElement>(tree);
        }

        public static IWebElement GetElementParentByClass(IWebElement element)
        {
            try
            {

                IWebElement parent = element;
                parent = parent.FindElement(By.XPath("./parent::*"));
                return parent;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool IsElementPresent_ByName(string elementName, ChromeDriver driver)
        {
            try { driver.FindElement(By.Name(elementName)); }
            catch (NoSuchElementException) { return false; }
            catch (StaleElementReferenceException) { return false; }
            return true;
        }

        public static bool IsAlertPresent(ChromeDriver driver)
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }   // try 
            catch (NoAlertPresentException Ex)
            {
                return false;
            }   // catch 
        }


        public static bool TryFindElementByClass(string elementClassName, ChromeDriver driver)
        {
            try
            {
                var element = driver.FindElementByClassName(elementClassName);
                var elementFound = element != null;
                return elementFound;
            }
            catch (NoSuchElementException e)
            {
               return false;
            }
        }
    }
}
