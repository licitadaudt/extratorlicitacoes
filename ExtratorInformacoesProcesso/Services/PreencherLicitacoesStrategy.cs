﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtratorInformacoesLicitacoes.Services
{
    public class PreencherLicitacoesStrategy
    {
        public IExtratorService ExtratorService { get; set; }
        public PreencherLicitacoesStrategy(IExtratorService extratorService)
        {
            ExtratorService = extratorService;
           
        }

        public void IniciarExtracao()
        {
            ExtratorService.IniciarGeracao();
        }
    }
}
