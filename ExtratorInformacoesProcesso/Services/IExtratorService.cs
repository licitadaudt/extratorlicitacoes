﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtratorInformacoesLicitacoes.Models;
using OpenQA.Selenium.Chrome;

namespace ExtratorInformacoesLicitacoes.Services
{
    public interface IExtratorService
    {
        bool Headless { get; set; }
        List<Licitacao> Licitacaos { get; set; }
        string Url { get; set; }
        bool ProsseguirExportacao { get; set; }

        void IniciarGeracao();
        void ProcessarConteudo(ChromeDriver driver, int dias, string termo);
        void PreencheLicitacoes(List<Licitacao> licitacoes, ChromeDriver driver);
       
    }
}
