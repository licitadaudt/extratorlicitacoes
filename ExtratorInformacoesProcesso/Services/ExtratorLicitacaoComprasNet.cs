﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtratorInformacoesLicitacoes.Models;
using ExtratorInformacoesLicitacoes.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ExtratorInformacoesLicitacoes.Services
{
   public class ExtratorLicitacaoComprasNet : IExtratorService
    {
        public bool Headless { get; set; }
        public List<Licitacao> Licitacaos { get; set; }
        public string Url { get; set; }
        public bool ProsseguirExportacao { get; set; }

        public ExtratorLicitacaoComprasNet(string url, bool headdles)
        {
            Url = url;
            Headless = headdles;
            Licitacaos = new List<Licitacao>();
        }

        public void IniciarGeracao()
        {
            var options = new ChromeOptions();
            var filtros = FiltroLicitacao.GetFiltros("extracao-comprasnet:", 1);

            if (Headless || filtros.Headless)
            {
                options.AddArgument("headless");
                //options.AddUserProfilePreference("plugins.always_open_pdf_externally", true);
            }

            using (var driver = new ChromeDriver(options))
            {

                Console.BackgroundColor = ConsoleColor.Blue;
                Console.WriteLine("Iniciando Extração das Licitações - BOT-CN");

                var url = Url;

                driver.Navigate().GoToUrl(url);

               
                ProsseguirExportacao = filtros.ProsseguirExportacao;
                if (!ProsseguirExportacao)
                {
                    Console.WriteLine($"Pulando Exportação - BOT-CN");
                    return;
                }

                Console.WriteLine($"Buscando para o filtro de {filtros.Dias} Dias");

                foreach (var termo in filtros.Termos)
                {
                    Console.WriteLine($"Iniciando busca para o termo {termo}");
                    ProcessarConteudo(driver, filtros.Dias, termo);
                    driver.Navigate().GoToUrl(url);
                }
            }
        }

        public void ProcessarConteudo(ChromeDriver driver, int dias, string termo)
        {
            var txtElaboracao = driver.FindElementByName("txtObjeto");
            txtElaboracao.SendKeys(termo);


            var chkModalidades = driver.FindElementsByName("chkModalidade");

            var auxChPregao = 0;

            foreach (var chkModalidade in chkModalidades)
            {
                if (auxChPregao == 4)
                {
                    chkModalidade.Click();
                }

                auxChPregao++;
            }

            var auxChPregaoEletronico = 0;

            var chkPregoesEletronicos = driver.FindElementByName("chk_pregaoTodos");
            chkPregoesEletronicos.Click();
          
            var dataInicial = DateTime.Now.AddDays(-dias).Date.ToString("dd/MM/yyyy");
            var dataFinal = DateTime.Now.Date.ToString("dd/MM/yyyy");

            Console.WriteLine($"Data inicial:{dataInicial}");
            Console.WriteLine($"Data final:{dataFinal}");

            var dtIniPublicacao = driver.FindElementById("dt_publ_ini");
            dtIniPublicacao.SendKeys(dataInicial);

            var dtFimPublicacao = driver.FindElementById("dt_publ_fim");
            dtFimPublicacao.SendKeys(dataFinal);

            var btnOk = driver.FindElementById("ok");
            btnOk.Click();



            PreencheLicitacoes(Licitacaos, driver);
        }

        public void PreencheLicitacoes(List<Licitacao> licitacoes, ChromeDriver driver)
        {
            var forms = driver.FindElementsByTagName("form");
            var auxFormsCount = 0;


            foreach (var form in forms)
            {
                var licitacao = new Licitacao();
                //como as informações estão em elementos com name que são equivalente a sua disposição nas tabelas
                //a variavel auxFormsCount é utilizada para sempre pegar as informações na proxima tabela.
                var link = form.FindElement(By.Name($"F{auxFormsCount}"));

                var table = link.FindElements(By.TagName("table"));

                var textoCompleto = table[0].FindElements(By.TagName("tr"))[2].FindElement(By.TagName("td"));
                var paragrafos = table[0].FindElements(By.TagName("tr"))[2].FindElement(By.TagName("td")).FindElements(By.TagName("b"));

                var codigoUasg = paragrafos[0].Text.Split(new[] { "Código da UASG: " }, StringSplitOptions.None)[1].Trim();

                //Quando a licitação não tem Abertura da Proposta vai dar um erro que será ignorado
                try
                {
                    var dataAbertura = textoCompleto.Text.Split(new[] { "Abertura da Proposta:" }, StringSplitOptions.None)[1].Trim().Replace("em", "").Trim().Substring(0, 10);
                    licitacao.DataAbertura = Convert.ToDateTime(dataAbertura);
                }
                catch
                {

                }


                licitacao.Objeto = textoCompleto.Text.Split(':')[3].Replace("Edital a partir de", "");
                licitacao.LicitacaoDescricao = paragrafos[1].Text;
                licitacao.Localidade = table[1].Text;
                licitacao.UASG = codigoUasg;

                var codigoLicitacao = licitacao.LicitacaoDescricao.Split(new[] { "Nº" }, StringSplitOptions.None)[1]
                    .Trim().Replace("/", "");

                licitacao.LinkDownlad = $"http://comprasnet.gov.br/ConsultaLicitacoes/download/download_editais_detalhe.asp?coduasg={licitacao.UASG}&modprp=5&numprp={codigoLicitacao}";
                licitacoes.Add(licitacao);

                auxFormsCount++;
            }


            var hasProximo = ChromeDriverUtil.IsElementPresent_ByName("btn_proximo", driver);
            if (hasProximo)
            {
                var btnProximo = driver.FindElementByName("btn_proximo");
                btnProximo.Click();

                Console.WriteLine("Buscando proxima pagina!");
                PreencheLicitacoes(licitacoes, driver);
            }
        }
    }
}
