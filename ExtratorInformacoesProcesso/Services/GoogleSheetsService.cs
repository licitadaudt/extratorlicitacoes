﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ExtratorInformacoesLicitacoes.Enuns;
using ExtratorInformacoesLicitacoes.Models;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;

namespace ExtratorInformacoesLicitacoes.Services
{
    public class GoogleSheetsService
    {
        private List<LicitacaoCompare> LicitacaoCompareList = new List<LicitacaoCompare>();
        private List<Licitacao> Licitacoes { get; set; }
        private TipoLicitacao TipoLicitacao { get; set; }

        public GoogleSheetsService(List<Licitacao> licitacoes, TipoLicitacao tipoLicitacao)
        {
            Licitacoes = licitacoes;
            TipoLicitacao = tipoLicitacao;
        }

        public void ProcessoAtualizacaoPlanilha()
        {
            var service = AuthorizeGoogleApp();
            ReadRows(service, Licitacoes);

            var licitacoesNovas = new List<Licitacao>();
            Licitacoes.ForEach(l =>
            {
                string codigoLicitacao;
                string compareFilter;
                bool licitacaoExistente;
                if (TipoLicitacao == TipoLicitacao.ComprasNet)
                {
                    codigoLicitacao = l.LicitacaoDescricao.Split(new[] { "Nº" }, StringSplitOptions.None)[1].Trim().Replace("/", "");
                    licitacaoExistente = LicitacaoCompareList.Any(lc => lc.CodigoLicitacao.Trim() == codigoLicitacao.Trim() && lc.CodigoUasg.Trim() == l.UASG.Trim());
                }
                else
                {
                    licitacaoExistente = LicitacaoCompareList.Any(lc => lc.Descricao.Trim() == l.Objeto.Trim());
                }

                if (!licitacaoExistente)
                {
                    licitacoesNovas.Add(l);
                }

            });

            InsertRows(service, licitacoesNovas);

            if (licitacoesNovas.Count > 0)
            {
                Console.WriteLine($"Enviando Tweet!");
                var twitter = new TwitterApi("Ph9JbXcYSMf3P6RtrgVkW5l3n", "DTEk7DleLYKNtWhzZzRleeTIHaVtzrmGWD4WWEYxrEjmWoQ5CF", "973956549017264128-2zvzgS34H6WCYj8zUgPH5ZFLw64AFfT", "l0sHPtbRSRwO2M9FECRa4oSWzDMxWgHqKoSUxbcsDHh8J");
                var response = twitter.Tweet($"Temos {licitacoesNovas.Count} novas oportunidades de melhorar o país. Confira no nosso painel de licitações em online.daudt.eng.br/licitacoes").Result;
                Console.WriteLine($"Enviando E-mail!");
                EnviarEmailService.EnviaMensagemEmail("contato@daudt.eng.br", "contato@daudt.eng.br", $"Atualização de painel de licitações - {DateTime.Now:dd/MM/yyy}", $"Painel de licitações atualizado com {licitacoesNovas.Count} itens em {DateTime.Now:dd/MM/yyy}");
            }

        }

        private SheetsService AuthorizeGoogleApp()
        {
            UserCredential credential;

            string[] Scopes = { SheetsService.Scope.Spreadsheets };
            var ApplicationName = "Licitacao Extrator";
            

            String serviceAccountEmail = "extratorlicitacoes@licitacoes.iam.gserviceaccount.com";


            var executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var configLocation = Path.Combine(executableLocation, "credentials.json");

            using (var stream =
                new FileStream(configLocation, FileMode.Open, FileAccess.Read))
            {
                var credPath = Path.Combine(executableLocation, "token.json");
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });


            return service;
        }

        protected void ReadRows(SheetsService service, List<Licitacao> licitacoes)
        {
            String spreadsheetId = "1vmOuYCVNsMWlSDQ7VghuhYmfsGJN9iZWFZUOINyGR74";
            String range = "B:H";

            SpreadsheetsResource.ValuesResource.GetRequest request =
                service.Spreadsheets.Values.Get(spreadsheetId, range);

            ValueRange response = request.Execute();
            IList<IList<Object>> values = response.Values;

            var firsLine = 0;
            if (values != null && values.Count > 0)
            {
                var validValues = values.Where(v => v.Count >= 4).ToList();
                foreach (var row in validValues)
                {
                    if (firsLine == 0)
                    {
                        firsLine++;
                        continue;
                    }

                    if (row.Count >= 6 && TipoLicitacao == TipoLicitacao.ComprasNet)
                    {
                        if (Convert.ToString(row[3]).Contains("Nº"))
                        {
                            var licitacaoCompare = new LicitacaoCompare();
                            licitacaoCompare.CodigoLicitacao =
                                Convert.ToString(row[3]).Split(new[] { "Nº" }, StringSplitOptions.None)[1]
                                    .Replace("/", "");
                            licitacaoCompare.CodigoUasg = Convert.ToString(row[5]);
                            licitacaoCompare.Descricao = Convert.ToString(row[1]);
                            LicitacaoCompareList.Add(licitacaoCompare);
                        }

                    }
                    else if (row.Count >= 5 && TipoLicitacao == TipoLicitacao.Tce)
                    {
                        var licitacaoCompare = new LicitacaoCompare();
                        licitacaoCompare.Descricao = Convert.ToString(row[1]);
                        LicitacaoCompareList.Add(licitacaoCompare);
                    }
                }
            }
            else
            {
                Console.WriteLine("No data found.");
            }

        }

        protected void InsertRows(SheetsService service, List<Licitacao> licitacoes)
        {
            String spreadsheetId = "1vmOuYCVNsMWlSDQ7VghuhYmfsGJN9iZWFZUOINyGR74";
            String range = "C:F";

            IList<IList<Object>> values = new List<IList<Object>>();

            foreach (var licitacao in licitacoes)
            {
                IList<Object> row = new List<Object>();
                row.Add("");
                row.Add("BOT");
                row.Add(licitacao.Objeto);

                row.Add(TipoLicitacao == TipoLicitacao.ComprasNet
                    ? licitacao.Localidade?.Trim().Remove(0, 2)
                    : licitacao.Localidade?.Trim());

                row.Add(licitacao.LicitacaoDescricao);
                row.Add(licitacao.DataAbertura.ToString("dd/MM/yyyy"));
                row.Add(licitacao.UASG);
                row.Add(string.Empty);
                row.Add(string.Empty);
                row.Add(string.Empty);
                row.Add(licitacao.LinkDownlad);
                values.Add(row);
            }

            SpreadsheetsResource.ValuesResource.AppendRequest request =
                service.Spreadsheets.Values.Append(new ValueRange() { Values = values }, spreadsheetId, range);
            request.InsertDataOption =
                SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.RAW;
            var response = request.Execute();


        }
    }
}