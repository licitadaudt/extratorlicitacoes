﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtratorInformacoesLicitacoes.Models;
using ExtratorInformacoesLicitacoes.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ExtratorInformacoesLicitacoes.Services
{
    public class ExtratorLicitacoesTce : IExtratorService
    {
        public bool Headless { get; set; }
        public List<Licitacao> Licitacaos { get; set; }
        public String Url { get; set; }
        public bool ProsseguirExportacao { get; set; }

        public ExtratorLicitacoesTce(string url, bool headdles)
        {
            Url = url;
            Headless = headdles;
            Licitacaos = new List<Licitacao>();
        }

        public void IniciarGeracao()
        {
            var options = new ChromeOptions();
            var filtros = FiltroLicitacao.GetFiltros("extracao-tce:", 0);
            if (Headless || filtros.Headless)
            {
                options.AddArgument("headless");
                //options.AddUserProfilePreference("plugins.always_open_pdf_externally", true);
            }

            using (var driver = new ChromeDriver(options))
            {

                Console.BackgroundColor = ConsoleColor.Blue;
                Console.WriteLine("Iniciando Extração das Licitações - BOT-RS");

                var url = Url;

                driver.Navigate().GoToUrl(url);

                
                ProsseguirExportacao = filtros.ProsseguirExportacao;
                if (!ProsseguirExportacao)
                {
                    Console.WriteLine($"Pulando Exportação - BOT-RS");
                    return;
                }

                Console.WriteLine($"Buscando para o filtro de {filtros.Dias} Dias");

                foreach (var termo in filtros.Termos)
                {
                    Console.WriteLine($"Iniciando busca para o termo {termo}");
                    ProcessarConteudo(driver, filtros.Dias, termo);
                    driver.Navigate().GoToUrl(url);
                }
            }

        }

        public void PreencheLicitacoes(List<Licitacao> licitacoes, ChromeDriver driver)
        {
            ChromeDriverUtil.WaitForJqueryAjax(driver);

            if(!ChromeDriverUtil.TryFindElementByClass("a-IRR-table", driver))
            {
                return;
            }

            var tabetaLicitacoes = driver.FindElementByClassName("a-IRR-table");
            var bodyTable = tabetaLicitacoes.FindElement(By.TagName("tbody"));
            var linhasTabela = bodyTable.FindElements(By.TagName("tr"));

            foreach (var linhaTabela in linhasTabela)
            {
                var colunasTabelas = linhaTabela.FindElements(By.TagName("td"));

                if (colunasTabelas.Count > 0)
                {
                    var licitacao = new Licitacao();
                    var hasLink = colunasTabelas[0].FindElement(By.ClassName("apex-edit-view"));

                    licitacao.LinkDownlad = colunasTabelas[0].FindElement(By.TagName("a")).GetAttribute("href");
                    licitacao.LicitacaoDescricao = colunasTabelas[1].Text + " N° " + colunasTabelas[2].Text + "/" + colunasTabelas[3].Text;
                    licitacao.DataAbertura = Convert.ToDateTime(colunasTabelas[6].Text);
                    licitacao.Objeto = colunasTabelas[4].Text;
                    licitacao.Localidade = "RS";
                    licitacoes.Add(licitacao);
                }
            }
        }

        public void ProcessarConteudo(ChromeDriver driver, int dias, string termo)
        {
            var txtElaboracao = driver.FindElementByClassName("a-IRR-search-field");
            txtElaboracao.SendKeys(termo);

            var btnOk = driver.FindElementByClassName("a-IRR-button--search");
            btnOk.Click();

            PreencheLicitacoes(Licitacaos, driver);
        }

       
    }
}
