﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExtratorInformacoesLicitacoes.Models
{
    public class FiltroLicitacao
    {
        public int Dias { get; set; }
        public List<string> Termos { get; set; }
        public bool ProsseguirExportacao { get; set; }
        public bool Headless { get; set; }

        public static FiltroLicitacao GetFiltros(string tipoExtracao, int lineIndex)
        {
            var filtro = new FiltroLicitacao
            {
                Termos = new List<string>()
            };

            var executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var configLocation = Path.Combine(executableLocation, "config.txt");
            var lines = File.ReadAllLines(configLocation, Encoding.ASCII);

            filtro.ProsseguirExportacao = Convert.ToBoolean(lines[lineIndex].Split(new[] { tipoExtracao }, StringSplitOptions.None)[1]);
            filtro.Headless = Convert.ToBoolean(lines[2].Split(new[] { "headless:" }, StringSplitOptions.None)[1]);
            filtro.Dias = Convert.ToInt32(lines[3].Split(new[] { "dias: " }, StringSplitOptions.None)[1]);

            filtro.Termos.AddRange(lines.Skip(5));

            return filtro;
        }
    }
}
