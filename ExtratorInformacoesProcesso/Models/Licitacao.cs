﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtratorInformacoesLicitacoes.Models
{
    public class Licitacao
    {
        public string Objeto { get; set; }
        public string Localidade { get; set; }
        public string LicitacaoDescricao { get; set; }
        public DateTime DataAbertura { get; set; }
        public string UASG { get; set; }
        public string LinkDownlad { get; set; }

    }
}
