﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtratorInformacoesLicitacoes.Models
{
    public class LicitacaoCompare
    {
        public string CodigoLicitacao { get; set; }
        public string CodigoUasg { get; set; }
        public string Descricao { get; set; }
    }
}
