﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtratorInformacoesLicitacoes.Enuns
{
    public enum TipoLicitacao
    {
        ComprasNet = 1,
        Tce = 2
    }
}
