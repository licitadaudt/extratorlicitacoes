﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using ExtratorInformacoesLicitacoes;
using ExtratorInformacoesLicitacoes.Enuns;
using ExtratorInformacoesLicitacoes.Models;
using ExtratorInformacoesLicitacoes.Services;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace ExtratorInformacoesProcesso
{
    class Program
    {
        private static List<Licitacao> licitacaoList = new List<Licitacao>();
        private static List<LicitacaoCompare> licitacaoCompareList = new List<LicitacaoCompare>();
        static void Main(string[] args)
        {
            IniciarGeracao();
        }

        private static void IniciarGeracao()
        {

            var extratorStrategyComprasNet = new PreencherLicitacoesStrategy(new ExtratorLicitacaoComprasNet("http://comprasnet.gov.br/ConsultaLicitacoes/ConsLicitacao_Filtro.asp",
                false));
            extratorStrategyComprasNet.IniciarExtracao();

            if (extratorStrategyComprasNet.ExtratorService.ProsseguirExportacao)
            {
                var googleSheetService = new GoogleSheetsService(extratorStrategyComprasNet.ExtratorService.Licitacaos, TipoLicitacao.ComprasNet);
                googleSheetService.ProcessoAtualizacaoPlanilha();
            }


            var extratorStrategyTce =  new PreencherLicitacoesStrategy(new ExtratorLicitacoesTce("http://www1.tce.rs.gov.br/aplicprod/f?p=50500:3:::NO", 
                                                                    false));
            extratorStrategyTce.IniciarExtracao();

            if (extratorStrategyTce.ExtratorService.ProsseguirExportacao)
            {
                var googleSheetService = new GoogleSheetsService(extratorStrategyTce.ExtratorService.Licitacaos, TipoLicitacao.Tce);
                googleSheetService.ProcessoAtualizacaoPlanilha();
            }
          
        }
    }
}

